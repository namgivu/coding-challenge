def solution(A, B):
    lenA    = len(A)
    lenB    = len(B)
    max_len = max(lenA,lenB)

    # initial A+B as :ab with all zero filled
    n = max_len + 1
    AB = [0]*n

    # ensure A and B has n+1 length same as :AB - for easier sum digit-by-digit later
    print(A); print(B)
    for i in range(0,n-lenA): A.append(0)
    for i in range(0,n-lenB): B.append(0)
    print(); print(A); print(B); print(AB)

    # calculate A+B
    r=0
    for i in range(0, n):
        s = A[i]+B[i] + r # s aka sum # r aka reminder

        AB[i] = s % 2
        r     = s // 2

    # the right-most zero will be trimmed
    if AB[n-1]==0: AB=AB[:n-2]

    print()
    return AB

print(solution(A=[0,1,1, 0,0,1, 0,1,1, 1,0,1, 0,1,1],
               B=[0,0,1, 0,0,1, 1,1,1, 1,0,1]))

'''
This is INCORRECT solution! Confirmed by NN!
'''