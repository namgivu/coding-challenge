def solution(A):
    list_length = 1 # counter for the target linked-list 's length

    i = 0 # start from the first node of the linked list at 0th aka linked-list's head
          # since A is NON-empty, we can always start looping with i=0 as below

    while True:
        next_node_index = A[i]
        if next_node_index != -1: # not ended yet
            list_length += 1
            i = next_node_index # we go to the next node in the list; since the input is assumed to be valid,
                                # by the assessment, we can always exit this loop
        else:
            return list_length


print(solution(A=[-1]))
print(solution(A=[1,-1]))
print(solution(A=[2,-1,1]))
print(solution(A=[2,-1,4,0,1]))
print(solution(A=[1,4,-1,3,2]))

'''
Only after reaching the end of assessment #2, I realize that I can read assessment 1 and 2 before I submit.
When I started, I thought I must go thru assessment #1 then assessment #2.
That's why I have submitted my in-progress solution for assessment #1

Here I would love to have my assessment #1 be updated by below code
Please consider to update my code for assessment #1 by this below one
Thank you and hope that I can show my best effort and commitment in hiring with Grab Singapore
Regards

# updated solution for assessment #1

'''