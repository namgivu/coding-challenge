#region solution_2nd
def solution_2nd(A, B):
    # convert bit to int for A
    intA=0 # this is X as mentioned in the question
    exp2A=1 # 2^i
    for b in A: # b aka bit
        intA += exp2A * b
        exp2A *= -2
    print(intA)

    # convert bit to int for B
    intB=0 # this is X as mentioned in the question
    exp2B=1 # 2^i
    for b in B: # b aka bit
        intB += exp2B * b
        exp2B *= -2
    print(intB)

    #region convert int to bit for AB - not minimized
    intAB = intA+intB
    print(intAB)
    AB=[]
    while intAB!=0:
        r = intAB % (-2)
        AB.append(abs(r))
        intAB = intAB // (-2)

    if len(AB)==0: AB=[0]
    return AB
    #endregion

    # region minimized effort - the should-be solution
    '''
    when having :intAB, the next step for me is to break it down into -2^i parts that have minimized length
    due to limited time, I cannot give the full code
    
    below is my strategy
    01 find the nearest -2^i value that closest to :intAB - save it as :closest_exp2
    02 intAB=intAB-closest_exp2
       record bit at :closest_exp2 as the result
    03 repeat step 01+02 until intAB=0 
    end
    
    sorry to miss the final code - with more time, I definitely can give better algorithm & code
    regards
    '''
    # endregion

print(solution_2nd(A=[0,1,1, 0,0,1, 0,1,1, 1,0,1, 0,1,1],
               B=[0,0,1, 0,0,1, 1,1,1, 1,0,1]))
             #AB=[0,1,1, 1,0,0, 0,0,1, 1,1,0, 1,1] #TODO not minimized?
             #   [0,1,0, 1,1,0, 0,0,1, 0,1,1, 1  ] #minimized value from the topic
#endregion

print('---------------------------------------------------------')

def solution(A, B):
    # convert bit to int for A
    intA  = 0 # this is X as mentioned in the assessment text
    exp2A = 1 # -2^i starts from 1
    for b in A: # b aka bit
        intA += exp2A * b
        exp2A *= -2

    # convert bit to int for B
    intB  = 0 # this is Y as mentioned in the assessment text
    exp2B = 1
    for b in B: # b aka bit
        intB += exp2B * b
        exp2B *= -2

    #region convert int to bit for AB
    max_len = max( len(A), len(B) )
    AB=[0] * (max_len+1)
    intAB = intA+intB

    # locate exp2 which greater than A+B ie :intAB
    exp2 = 1; i=0
    cache = {} # storing the -2^i here i: -2^i
    while abs(exp2)<intAB:
        cache[i] = exp2
        exp2 *= -2
    # reaching here means we have abs(exp2) > intAB at -2^i

    s = intAB
    while s!=0:
        # locate the 2^i greater than :s
        while abs(cache[i])<s: i-=1
        # reaching here means we have abs(cache[i])>s

        # which 2^i is closer to :s
        exp_i      = cache[i]
        exp_i_prev = cache[i-1]
        distance_i      = abs( abs(exp_i)-s )
        distance_i_prev = abs( abs(exp_i_prev)-s )
        i_closer = i if distance_i<distance_i_prev else i-1

        # deduct :s and go on the break down until get :s zero
        s -= cache[i_closer]
        i  = i_closer
        print()

    #endregion

    '''
       i        0   1   2    3   4    5      6     7      8        9      10       11     12       13      14       15  
    -2^i        1   -2  4   -8  16  -32     64  -128    256     -512    1024    -2048   4096    -8192   16384   -32768
    5730    A   0   1   1    0   0    1      0     1      1        1       0        1      0        1       1  
    -2396   B   0   0   1    0   0    1      1     1      1        1       0        1
    3334    -   0   1   0    1   1    0      0     0      1        0       1        1      1

    4096-3334
    762
    8192-3334
    4858
    2048-3334
    -1286

    so go for 4096 closer at 762

    continue with -762
    '''

    return AB

print(solution(A=[0,1,1, 0,0,1, 0,1,1, 1,0,1, 0,1,1 ],
               B=[0,0,1, 0,0,1, 1,1,1, 1,0,1        ]))
#                [0,1,0, 1,1,0, 0,0,1, 0,1,1, 1     ] #minimized value from the topic

#TODO copy this code to assessment #2 's note - at the end of the file

print('---------------------------------------------------------')


def bit_to_int(a):
    e2=1
    s=0
    for b in a:
        s += e2*b
        e2 *= -2
    return s

print(bit_to_int([0,1,1, 0,0,1, 0,1,1, 1,0,1, 0,1,1 ]))
print(bit_to_int([0,0,1, 0,0,1, 1,1,1, 1,0,1        ]))
print(bit_to_int([0,1,0, 1,1,0, 0,0,1, 0,1,1, 1     ]))
print(bit_to_int([0,1,0, 1,1,0, 0,0,1, 0,1,1        ]))