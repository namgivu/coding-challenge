topic
https://app.codility.com/c/run/trainingQ862DG-EFZ/

submission
https://app.codility.com/demo/results/trainingQ862DG-EFZ/

# code 
solution 0th
```python
def solution(N, A):
    c  = [0]*N # c aka counter # initial :c as filled by zero
    mx = 0     # mx aka max
    for a in A:
        if a!=N+1:
            i=a-1
            c[i]+=1
            mx = max(mx, c[i])
        else:
            for i in range(0,N): c[i]=mx
        print(c)
    return c

print(solution(N=5, A=[3,4,4,6,1,4,4]))
```

solution 2nd #TODO incorrect!
```python
def solution(N, A):
    c  = [0]*N # c aka counter # initial :c as filled by zero
    mx = 0     # mx aka max
    for a in A:
        if a!=N+1: # action = increase 1
            i=a-1
            if c[i]>=last_maxall:
                c[i]+=1
            else:
                c[i]=last_maxall+1
            mx = max(mx, c[i])
        
        else: # action = set max for all
            
            # for i in range(0,N): c[i]=mx 
            '''solution 0th just to make a loop thru all counter in :c and set the value --> not effective with big-size :c'''
            
            # solution 2nd with effective-focused
            last_maxall = mx

        print(c)
    return c

print(solution(N=5, A=[3,4,4,6,1,4,4]))

```
