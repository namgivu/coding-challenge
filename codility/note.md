tips
* TODO how to set default programming language as Python please

* (google for the code/algorithm)
  if you get stuck at finding the right solution/algorithm, why DON'T YOU GOOGLE for it?

* (start from the easiest one strategy)
  if you have multiple questions in the challenge,
  recommended that you MUST GO THRU ALL the questions 
  and start your submissions from the easiest one i.e. the most painful one goes last

* useful hotkey
  ctrl-D delete whole line
  ctrl-Z undo
  
* code directly on codility with lovely testcases
* open pycharm to have python console ready to run code snippet

* when a failed testcase occurs, keep run-and-failing on codility will decrease your effectiveness
  --> emulate the testcase on pycharm's python console and run there to fix the issue
  when all good, run again on codility 
  then, submit of course when all passed

* even though codility site saying a submit solution cannot be modified later,
  we can re-upload the solution by browsing the lesson chalenge again and hit Start
