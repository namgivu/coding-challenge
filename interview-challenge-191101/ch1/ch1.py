def deleteProducts(ids, m):
    count = {}  # c aka count_occurrence
    for id in ids:
        if id in count: count[id]+=1
        else:            count[id]=1

    s = {k:count[k] for k in sorted(count, key=count.get)}  # s aka count_sorted_by_value - sort dict by value ref. https://stackoverflow.com/a/20948781/248616
    del count

    can_deleted = m
    deleted = 0
    for to_delete in s.values():
        if to_delete <= can_deleted:
            can_deleted -= to_delete
            deleted += 1
        else:
            break  # no need to go further ie all the next :to_delete will > can_delete

    different_id_total    = len(s)
    different_id_remained = different_id_total - deleted
    return different_id_remained

INPUT='input.txt'
with open(INPUT, 'r') as f:
    L = f.readlines(); L = [l.strip() for l in L]

    n = int(L[0])
    ids = [int(L[i]) for i in range(1, 1 + n)]
    m = int(L[n + 1])

r = deleteProducts(ids, m)
print(r)
