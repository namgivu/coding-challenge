n = None
solution  = None
activated = []

def is_all_garden_covered(activated:'0/1 bit array', coverage: 'dict[i] = coverage at i') -> bool:
    """
    each C=coverage[i] where activated[i]==1
    C generates a range [minC, maxC]
    union all the ranges and when we can have [0, n], we have all the garden
    """
    global n; assert n == len(coverage)

    ranges = []
    for i, bit in enumerate(activated):
        C      = coverage[i]
        minC   = max(i-C, 1)    # ref. the coverage formula in defined topic
        maxC   = min(i+C, n-1)  # ref. the coverage formula in defined topic
        rangeC = [minC, maxC]
        ranges.append(rangeC)

    # do union all ranges ref. https://stackoverflow.com/a/15273749/248616
    u = []  # u aka union-ed range(s)
    for begin,end in sorted(ranges):
        if u and u[-1][1] >= begin - 1:
            u[-1][1] = max(u[-1][1], end)
        else:
            u.append([begin, end])

    ok = True
    ok = ok and len(u) == 1
    ok = ok and u[0][0]==0 and u[1][1]==n-1
    return ok

def recursive_set_01_bit__then_check_coverage(_at, coverage: 'dict[i] = coverage at i'):
    global n, activated, solution
    if solution is not None: return # found solution, just end it
    if _at >= n: # we have reached _at = n, do checking if all covered
        checked = is_all_garden_covered(activated, coverage)
        if checked is True:
            solution = n
            return
        else:
            return

    for bit in (0,1):
        # try this candidate bit
        activated[_at] = bit

        # go recursively to next position
        recursive_set_01_bit__then_check_coverage(_at + 1, coverage)

        # go-out cleaning
        activated[_at] = None

def find(coverage: 'dict[i] = coverage at i'):
    """
    we brute-force thru all subsets size=subset_size and see if it cover all the garden when activated
    """
    recursive_set_01_bit__then_check_coverage(_at=0, coverage=coverage)

def fountainActivation(locations):
    # sort coverage
    d = { i:cover for i, cover in enumerate(locations) }
    c = {k:d[k] for k in sorted(d, key=d.get, reverse=True)}  # c aka coverage - sort dict by value ref. https://stackoverflow.com/a/20948781/248616
    del d

    # find solution
    global n; assert n == len(locations)
    global activated; activated = [ None for i in range(n) ]
    find(coverage=c)
    if solution is not None:
        return solution  # found solution!
    else:
        # worse case, all activated
        return n

INPUT='input.txt'
with open(INPUT, 'r') as f:
    L = f.readlines(); L = [l.strip() for l in L]

    n = int(L[0])
    locations = [int(L[i]) for i in range(1, 1 + n)]

r = fountainActivation(locations)
print(r)
