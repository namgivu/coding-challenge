My solution follows below steps
* parse each box in :boxList into box objects
  each box object contain box :identifier, and box :version

* base on box :version, we categorize the box to be the newer/older generation

* we sort the older-generation boxes by two fields in below order
  1st field is box :version
  2nd field is then by box :identifier when 1st-field ties
  
* we output the sorted older-generation boxes, 
  then output newer-generation right after
