"""
box = identifier + version info

older version = lowercase string
newer version = positive number

sort
    older boxes first, alphabetical order for version
        if ties, sort by identifier
    newer boxes with original order
"""


def is_newer_box(version):
    splits = version.split(' ')
    for s in splits:
        try:
            val = int(s) # check if :s is numeric ref. https://stackoverflow.com/a/19440971/248616
        except ValueError:
            return False
    return True

def orderedJunctionBoxes(numberOfBoxes, boxList):
    older_box_items=[]
    newer_box_indexes=[]

    # into newer & older group
    for i in range(0,numberOfBoxes):
        box=boxList[i]
        identifier, version = box.split(' ', 1)

        if is_newer_box(version):
            newer_box_indexes.append(i)
        else:
            older_box_items.append([version, identifier, i])

    # sort older group
    '''sort by multi fields ref. https://stackoverflow.com/a/4233482/248616'''
    older_box_items = sorted(older_box_items, key=lambda x: (x[0], x[1]))

    # output
    r=[]
    for obi in older_box_items: r.append(boxList[ obi[2] ])
    for i in newer_box_indexes: r.append(boxList[i])

    pass
    return r


orderedJunctionBoxes(numberOfBoxes=6, boxList=[
    'ykc 82 01',
    'eo first qpx',
    '09z cat hamster',
    '06f 12 25 6',
    'az0 first qpx',
    '236 cat dog rabbit snake',
])
import sys; sys.exit()

orderedJunctionBoxes(numberOfBoxes=6, boxList=[
    ['ykc', '82',       '01'],
    ['eo',  'first',    'qpx'],
    ['09z', 'cat',      'hamster'],
    ['06f', '12',       '25',   '6'],
    ['az0', 'first',    'qpx'],
    ['236', 'cat',      'dog',  'rabbit', 'snake'],
])
pass
