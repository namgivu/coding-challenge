import sys

def getMinimumCostToRepair(numTotalAvailableCities,

                           numTotalAvailableRoads,
                           roadsAvailable,

                           numRoadsToBeRepaired,
                           costRoadsToBeRepaired
):
    n = numTotalAvailableCities # alias for N
    if n==0 or n==1: return 0

    a = [0]*n # the N*N graph matrix
    for i in range(0, n): a[i] = [0]*n

    # build graph matrix :a by -1 marker
    for i in range(0, numTotalAvailableRoads):
        c1, c2   = roadsAvailable[i] # c1, c2 for city 1 and 2
        c1-=1; c2-=1

        # mark as the c1 and c2 connected - marked as -1
        a[c1][c2] = -1
        a[c2][c1] = -1

    # add cost to city-edge to :a marker as the positive-int cost value
    for i in range(0, numRoadsToBeRepaired):
        c1,c2,cost = costRoadsToBeRepaired[i]
        c1-=1; c2-=1
        a[c1][c2] = a[c2][c1] = cost

    #region get networks created by the broken roads
    networks = [None]*n
    network_index = -1
    for i in range(0,n):
        if networks[i] is None: # not process for city :i
            network_index += 1

            # identify new network from :i
            networks[i] = network_index
            cities1=[i]
            cities2=[]
            while len(cities1)>0:
                for c1 in cities1: # find links from :cities1
                    for c2 in range(0, n):
                        if networks[c2] is None:  # not process for city :c2
                            if a[c1][c2]==-1 or a[c2][c1]==-1:
                                cities2.append(c2)
                                networks[c2] = network_index

                # here, we have seeked from cities1, found linked cities are stored in cities2

                cities1=cities2 # continue seeking from :cities2 again
                cities2=[]

            # here, we cannot find any new linked city for network at :i and :network_index

    network_cities={} # 'network_index: [city1, city2, ...]
    for c in range(0, n):
        network_index = networks[c]
        if not network_cities.get(network_index):
            network_cities[network_index]=[]
        network_cities[network_index].append(c)
    #endregion get networks created by the broken roads

    #region we find the minimum cost to repair the disconnected networks based on similar-Dijkstra-algorithm in graph theory
    m = network_index+1 # number of networks found
    b = [sys.maxint]*m # b the graph matrix between the networks
    for ni in range(0, m): b[ni] = [0]*m

    '''from here we will find the minimum edges in matrix :b that connect all nodes/networks together with minimum cost'''
    # first, we build graph matrix between the network nodes as :b
    for ni in range(0, m):
        for nj in range(0, m):
            # find :min_cost_ij as minimum cost to link network :i and :j
            min_cost_ij = sys.maxint # max int ref. https://stackoverflow.com/a/7604981
            for i in network_cities[ni]:
                for j in network_cities[nj]:
                    if a[i][j]>0:
                        min_cost_ij = min(min_cost_ij, a[i][j])
            b[ni][nj] = b[nj][ni] = min_cost_ij

    # we seek the minimum covering-skeleton that link all networks
    done = [False]*n
    r=0
    covering_skeleton=[0]
    done[0]=True
    for _ in range(0,m-1): # repeat n times
        min_cost_n1n2 = sys.maxint
        new_found = None
        for n1 in covering_skeleton: # n1 aka network1
            for n2 in range(0, m): # n2 aka network2
                if not done[n2]:
                    if b[n1][n2]!=sys.maxint:
                        min_cost_n1n2 = min(min_cost_n1n2, b[n1][n2])
                        new_found = n2
        if new_found:
            r += min_cost_n1n2
            done[new_found] = True
            covering_skeleton.append(new_found)
        else:
            raise Exception('Something not right - we should not be here')
    #endregion we find the minimum cost to repair the disconnected networks

    pass
    return r


getMinimumCostToRepair(numTotalAvailableCities=5,

                       numTotalAvailableRoads=5,
                       roadsAvailable=[[1,2],[2,3],[3,4],[4,5],[1,5]],

                       numRoadsToBeRepaired=3,
                       costRoadsToBeRepaired=[[1,2,12],[3,4,30],[1,5,8]]
) # expect output to be 20
