topic
question 2 at https://www.myamcat.com/start-demo?data=VvSY8gb%2BBAAefK0phJWENBOPoTXDc8na4fhRROtwuZ9MIbQ7gXB2pLYyDf%2B4UUy3bjNJmcgRqJuzDgML1nQF%2Bg%3D%3D

code 
```python

def gcd(x, y):
    """ref.https://www.geeksforgeeks.org/gcd-in-python/"""
    while(y):
        x, y = y, x % y
    return x

def generalizedGCD(num, arr):
    if num<=0:  return 1
    if num==1: return arr[0]

    g=gcd(arr[0],arr[1])
    for i in range(2, num):
        g=gcd(g,arr[i])

    return g


print(generalizedGCD(num=-1, arr=[]))
print(generalizedGCD(num=0, arr=[]))
print(generalizedGCD(num=1, arr=[122]))
print(generalizedGCD(num=2, arr=[2,3]))
print(generalizedGCD(num=2, arr=[12,30]))

print(generalizedGCD(num=5, arr=[2,3,4,5,6]))
print(generalizedGCD(num=5, arr=[2,4,6,8,10]))
```
