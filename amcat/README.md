Force to use python 2.7.16 as amcat's env is setup so 
```bash

# install latest version for 2.7 here
latest27=`pyenv install --list | grep -E '^[ ]*2.7' | tail -n1` # get latest version
echo "installing latest python 2.7 as $latest27"
pyenv install -s $latest36 # do install # -s aka -skip-existing

# set project's python version to be 2.7.16
pyenv local 2.7.16 
pipenv install --python 2.7.16 # ref. https://pipenv.readthedocs.io/en/latest/basics/#specifying-versions-of-python
``` 
